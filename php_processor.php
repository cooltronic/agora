<?php

use function GuzzleHttp\Psr7\copy_to_string;

require __DIR__ . '/vendor/autoload.php';

// load file;

$filename = $argv[1];

if($filename == null){

    echo ("E1: Filename argument in CLI is not specified");
    exit;

}

if (!file_exists($filename)) {

    echo ("E2: File does not exist");
    exit;

}

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($filename);
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($filename);

// read file
foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {

    $maxCell = $worksheet->getHighestRowAndColumn();
    $data = $worksheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
    $filtered = array_map('array_filter', $data); // filter and cut worksheet data to useful space
    $filtered = array_filter($filtered);

}

if (!(isset($filtered[0][0])
&&isset($filtered[0][1])
&&isset($filtered[0][2])
&&isset($filtered[0][3])
&&isset($filtered[0][4]))) {

    echo ("E5: Empty header line or spreadsheet");
    exit;

}

if (!($filtered[0][0]=="Spedytor"
&&$filtered[0][1]=="Nr listu przewozowego"
&&$filtered[0][2]=="Kwota"
&&$filtered[0][3]=="Klient"
&&$filtered[0][4]=="Nr zamówienia")) {

    echo ("E3: Header structure are different than known");
    exit;

}

unset($filtered[0]);
sort($filtered);

// get client list
foreach ($filtered as $value)
{

    $client[] = $value[3];

}

$clients = array_unique($client); // get client unique values
sort($clients);

// build json
for ($i=0; $i < count($clients) ; $i++){

    $clientName=$clients[$i];
    $x = 0;
    $sum[$i] = 0;

    foreach ($filtered as $value)
    {

        if ($value[3] == $clientName){

            // Malformed input data checking
            if (!isset($value[0])){

                echo ("M4: Some values are malformed: courier\nPosition skipped!\n");
                echo ("client:".$clientName."\nvalues:");
                print_r($value);
                continue;

            }

            if (!isset($value[1])){

                echo ("M4: Some values are malformed: waybill_number\nPosition skipped!\n");
                echo ("client:".$clientName."\nvalues:");
                print_r($value);
                continue;

            }

            if (!isset($value[2])){

                echo ("M4: Some values are malformed: amount\nPosition skipped!\n");
                echo ("client:".$clientName."\nvalues:");
                print_r($value);
                continue;

            }

            if (!isset($value[4])){

                echo ("M4: Some values are malformed: order_number\nPosition skipped!\n");
                echo ("client:".$clientName."\nvalues:");
                print_r($value);
                continue;

            }

            $singlePosition['amount'] = $value[2];
            $sum[$i] = $sum[$i] + $value[2];
            $singlePosition['courier'] = $value[0];
            $singlePosition['waybill_number'] = $value[1];
            $singlePosition['order_number'] = $value[4];
            $positions[$x]=$singlePosition;
            $x++;

        }

    }
    
    $json_data=["data"=> [$positions], "sum" => $sum[$i]]; 
    $fp = fopen($clientName."_report_".date("Ymdhis").".json", "w"); // save file with filename
    fwrite($fp, json_encode($json_data));
    fclose($fp);

}
